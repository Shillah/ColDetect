#include "scm_triangle.h"

#include <libguile.h>

#include "scm_points.h"

// TODO: make the gc clean up the objects

static scm_t_bits triangle_tag;


struct triangle *
make_triangle( struct point *a, struct point *b, struct point *c )
{
        struct triangle *tria = malloc( sizeof( struct triangle ) );

        double side_a = point_dist( b, c );
        double side_b = point_dist( a, c );
        double side_c = point_dist( a, b );
        
        tria->a = a;
        tria->b = b;
        tria->c = c;

        tria->side_a = side_a;
        tria->side_b = side_b;
        tria->side_c = side_c;

        return tria;
}

SCM
s_make_triangle( SCM s_a, SCM s_b, SCM s_c )
{
        SCM smob;
        struct triangle *tria;
        
        struct point *a = get_point( s_a );
        struct point *b = get_point( s_b );
        struct point *c = get_point( s_c );

        tria = make_triangle( a, b, c );

        smob = scm_new_smob( triangle_tag, (long) tria );

        return smob;
}

SCM
s_triangle_a( SCM s_tria )
{
        scm_assert_smob_type( triangle_tag, s_tria );
        struct triangle *tria = (struct triangle *) SCM_SMOB_DATA( s_tria );
        struct point *a = triangle_a( tria );
        return scm_new_smob( s_point_tag( ), (long) a );
}

SCM
s_triangle_b( SCM s_tria )
{
        scm_assert_smob_type( triangle_tag, s_tria );
        struct triangle *tria = (struct triangle *) SCM_SMOB_DATA( s_tria );
        struct point *b = triangle_b( tria );
        return scm_new_smob( s_point_tag( ), (long) b );
}

SCM
s_triangle_c( SCM s_tria )
{
        scm_assert_smob_type( triangle_tag, s_tria );
        struct triangle *tria = (struct triangle *) SCM_SMOB_DATA( s_tria );
        struct point *c = triangle_c( tria );
        return scm_new_smob( s_point_tag( ), (long) c );
}

SCM
s_triangle_side_a( SCM s_tria )
{
        scm_assert_smob_type( triangle_tag, s_tria );
        struct triangle *tria = (struct triangle *) SCM_SMOB_DATA( s_tria );
        float side_a = triangle_side_a( tria );
        return scm_from_double( side_a );
}

SCM
s_triangle_side_b( SCM s_tria )
{
        scm_assert_smob_type( triangle_tag, s_tria );
        struct triangle *tria = (struct triangle *) SCM_SMOB_DATA( s_tria );
        float side_b = triangle_side_b( tria );
        return scm_from_double( side_b );
}

SCM
s_triangle_side_c( SCM s_tria )
{
        scm_assert_smob_type( triangle_tag, s_tria );
        struct triangle *tria = (struct triangle *) SCM_SMOB_DATA( s_tria );
        float side_c = triangle_side_c( tria );
        return scm_from_double( side_c );
}

int
print_triangle( SCM s_tria, SCM port, scm_print_state* pstate )
{
        scm_assert_smob_type( triangle_tag, s_tria );
        struct triangle *tria = (struct triangle *) SCM_SMOB_DATA( s_tria );
        scm_simple_format( port,
                           scm_from_locale_string(
                                   "#<triangle (~A, ~A), (~A, ~A), (~A, ~A)>"),
                           scm_list_n( scm_from_double( tria->a->x ),
                                       scm_from_double( tria->a->y ),
                                       scm_from_double( tria->b->x ),
                                       scm_from_double( tria->b->y ),
                                       scm_from_double( tria->c->x ),
                                       scm_from_double( tria->c->y ),
                                       SCM_UNDEFINED ) );
        scm_remember_upto_here_1( s_tria );
        return 0;
}

SCM
equal_triangle( SCM s_t1, SCM s_t2 )
{
        struct triangle *t1, *t2;
        scm_assert_smob_type( triangle_tag, s_t1 );
        scm_assert_smob_type( triangle_tag, s_t2 );
        t1 = (struct triangle *) SCM_SMOB_DATA( s_t1 );
        t2 = (struct triangle *) SCM_SMOB_DATA( s_t2 );
        scm_remember_upto_here_1( s_t1 );
        scm_remember_upto_here_1( s_t2 );
        if ( (t1->a->x == t2->a->x && t1->a->y == t2->a->y) &&
             (t1->b->x == t2->b->x && t1->b->y == t2->b->y) &&
             (t1->c->x == t2->c->x && t1->c->y == t2->c->y) )
                return SCM_BOOL_T;
        else
                return SCM_BOOL_F;
}

void
init_triangle_type( void )
{
        init_point_type( );
        
        /* 
         * Setup the point smob
         */
        
        triangle_tag = scm_make_smob_type( "triangle",
                                           sizeof( struct triangle ) );
        //scm_set_smob_free( point_tag, free_point );
        scm_set_smob_print( triangle_tag, print_triangle );
        scm_set_smob_equalp( triangle_tag, equal_triangle );

        /*
         * Setup the point routines
         */

        scm_c_define_gsubr( "make-triangle", 3, 0, 0, s_make_triangle );
        scm_c_define_gsubr( "triangle-a", 1, 0, 0, s_triangle_a );
        scm_c_define_gsubr( "triangle-b", 1, 0, 0, s_triangle_b );
        scm_c_define_gsubr( "triangle-c", 1, 0, 0, s_triangle_c );
        scm_c_define_gsubr( "triangle-side-a", 1, 0, 0, s_triangle_side_a );
        scm_c_define_gsubr( "triangle-side-b", 1, 0, 0, s_triangle_side_b );
        scm_c_define_gsubr( "triangle-side-c", 1, 0, 0, s_triangle_side_c );
}
