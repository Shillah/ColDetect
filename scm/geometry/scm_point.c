#include "scm_point.h"

#include <libguile.h>

/*
 * SCM Interface
 */
static scm_t_bits point_tag;

SCM
make_point( SCM s_x, SCM s_y )
{
        SCM smob;
        struct point *point;
        double x = scm_to_double( s_x );
        double y = scm_to_double( s_y );
        point = (struct point *)
                malloc( sizeof( struct point ) );

        point->x = x;
        point->y = y;

        smob = scm_new_smob( point_tag, (long) point );

        return smob;
}

struct point *
get_point( SCM s_p )
{
        scm_assert_smob_type( point_tag, s_p );

        struct point *point = (struct point *) SCM_SMOB_DATA( s_p );

        scm_remember_upto_here_1( s_p );
        
        return point;
}

int
print_point( SCM s_point, SCM port, scm_print_state* pstate )
{
        scm_assert_smob_type( point_tag, s_point );
        struct point *point = (struct point *) SCM_SMOB_DATA( s_point );
        scm_simple_format( port, scm_from_locale_string("(~A, ~A)"),
                           scm_list_2( scm_from_double( point->x ),
                                       scm_from_double( point->y )) );
        scm_remember_upto_here_1( s_point );
        return 0;
}

SCM
equal_point( SCM s_p1, SCM s_p2 )
{
        struct point *p1, *p2;
        scm_assert_smob_type( point_tag, s_p1 );
        scm_assert_smob_type( point_tag, s_p2 );
        p1 = (struct point *) SCM_SMOB_DATA( s_p1 );
        p2 = (struct point *) SCM_SMOB_DATA( s_p2 );
        scm_remember_upto_here_1( s_p1 );
        scm_remember_upto_here_1( s_p2 );
        if ( p1->x == p2->x && p1->y == p2->y )
                return SCM_BOOL_T;
        else
                return SCM_BOOL_F;
}

SCM
s_point_x( SCM s_p )
{
        scm_assert_smob_type( point_tag, s_p );
        struct point *p = (struct point *) SCM_SMOB_DATA( s_p );
        return scm_from_double( point_x( p ) );
}

SCM
s_point_y( SCM s_p )
{
        scm_assert_smob_type( point_tag, s_p );
        struct point *p = (struct point *) SCM_SMOB_DATA( s_p );
        return scm_from_double( point_y( p ) );
}

SCM
s_point_len( SCM s_p )
{
        scm_assert_smob_type( point_tag, s_p );
        struct point *p = (struct point *) SCM_SMOB_DATA( s_p );
        return scm_from_double( point_len( p ) );
}

SCM
s_point_dist( SCM s_p1, SCM s_p2 )
{
        scm_assert_smob_type( point_tag, s_p1 );
        scm_assert_smob_type( point_tag, s_p2 );
        struct point *p1 = (struct point *) SCM_SMOB_DATA( s_p1 );
        struct point *p2 = (struct point *) SCM_SMOB_DATA( s_p2 );
        return scm_from_double( point_dist( p1, p2 ) );
}

void
init_point_type( void )
{
        static int initialized = 0;

        if (initialized == 0)
        {
                initialized = 1;
                /* 
                 * Setup the point smob
                 */
                
                point_tag = scm_make_smob_type( "point",
                                                sizeof( struct point ) );
                scm_set_smob_print( point_tag, print_point );
                scm_set_smob_equalp( point_tag, equal_point );

                /*
                 * Setup the point routines
                 */

                scm_c_define_gsubr( "make-point", 2, 0, 0, make_point );
                scm_c_define_gsubr( "point-x", 1, 0, 0, s_point_x );
                scm_c_define_gsubr( "point-y", 1, 0, 0, s_point_y );
                scm_c_define_gsubr( "point-len", 1, 0, 0, s_point_len );
                scm_c_define_gsubr( "point-dist", 2, 0, 0, s_point_dist );
        }
}
