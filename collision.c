#include <stdio.h>
#include <stdlib.h>
#include <libguile.h>
#include <math.h>

#include "point.h"
#include "triangle.h"

        
static void
inner_main( void *data, int argc, char **argv )
{
        init_point_type( );
        init_triangle_type( );
        scm_shell( argc, argv );
}

int
main( int argc, char **argv )
{
        scm_boot_guile( argc, argv, inner_main, 0 );
        return 0;
}
