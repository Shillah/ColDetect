NAME=ColDetect

all: collision.o point.o triangle.o
	gcc -o $(NAME) $^ `pkg-config --libs guile-2.0` -lm

%.o: %.c
	gcc -c -o $@ $^ `pkg-config --cflags guile-2.0`
