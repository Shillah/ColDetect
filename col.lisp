(defpackage :collision
  (:use :common-lisp))
(in-package :collision)

(defstruct (point (:constructor p (x y)))
  x y)

(defun sq (x)
  "Squares x"
  (* x x))

(defun point-abs (A)
  "Computes the distance from point A to 0"
  (sqrt (+ (sq (point-x A)) (sq (point-y A)))))

(defun point-distance (A B)
  "Computes the distance between point A and point B"
  (point-abs (p (- (point-x A) (point-x B)) (- (point-y A) (point-y B)))))

(defstruct (triangle (:constructor make-triangle
                                   (A B C &aux
                                      (side-a (point-distance B C))
                                      (side-b (point-distance A C))
                                      (side-c (point-distance A B)))))
  (A nil :type point)
  (B nil :type point)
  (C nil :type point)
  (side-a 0.0 :type float)
  (side-b 0.0 :type float)
  (side-c 0.0 :type float))

(defstruct (box (:constructor make-box (bot-left height width)))
  (bot-left nil :type point)
  (height 0.0 :type float)
  (width 0.0 :type float))

(defstruct polygon
  triangle-list
  (bounding-box nil :type box))

(defun weak-collision-check (A B)
  "Checks wether the bounding boxes of A and B collide"
  (let ((box-1 (polygon-bounding-box A))
        (box-2 (polygon-bounding-box B)))
    (box-box-col box-1 box-2)))

(defun interval-check (a b c d)
  (let ((r-a (min a b))
        (r-b (max a b))
        (r-c (min c d))
        (r-d (max c d)))
    (not (or (< r-b r-c)
             (< r-d r-a)))))

(defun box-box-col (box-1 box-2)
  (let ((p-1 (box-bot-left box-1))
        (p-2 (box-bot-left box-2)))
    (let ((x-1 (point-x p-1)) (y-1 (point-y p-1))
          (x-2 (point-x p-2)) (y-2 (point-y p-2)))
      (and (interval-check x-1 (+ x-1 (box-width box-1))
                           x-2 (+ x-2 (box-width box-2)))
           (interval-check y-1 (+ y-1 (box-height box-1))
                           y-2 (+ y-2 (box-height box-2)))))))

(defun point-box-col (point box)
  (let ((x-1 (point-x point))
        (y-1 (point-y point))
        (x-2 (point-x (box-bot-left box)))
        (y-2 (point-y (box-bot-left box)))
        (w (box-width box))
        (h (box-height box)))
    (and (< x-2 x-1 (+ x-2 w))
         (< y-2 y-1 (+ y-2 h)))))

(defun tri-box-col (tria box)
  (or (point-box-col (triangle-a tria) box)
      (point-box-col (triangle-b tria) box)
      (point-box-col (triangle-c tria) box)))

(defun tri-tri-col (tria-1 tria-2)
  nil)
    
