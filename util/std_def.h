#if !defined( STD_DEF_H__ )
#define STD_DEF_H__

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

typedef unsigned int index;
#define INDEX_TO_P( x ) ((void *) (uintptr_t) (x))
#define P_TO_INDEX( x ) ((index) (uintptr_t) (x))

#endif /* STD_DEF_H__ */
