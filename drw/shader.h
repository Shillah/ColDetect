#if !defined(SHADER_H__)
#define SHADER_H__

#include <GL/glew.h>

struct shader_info
{
        GLenum type;
        char const *name;
        GLuint shader;
};

GLuint
load_shaders( struct shader_info pipeline[] );
        
#endif /* SHADER_H__ */
