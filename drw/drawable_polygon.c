#include <drw/drawable_polygon.h>

#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <util/std_def.h>
#include <geometry/polygon.h>

#define BUFFER_OFFSET(x) \
        ((void const *) (x))

struct dpol *
make_dpol( struct polygon *pol )
{
        for ( index i = 0; i < pol->num_points-2; ++i )
        {
                printf( "TRIANGLE\n" );
                printf( "%u %u %u\n",
                        pol->triangles[3*i+0],
                        pol->triangles[3*i+1],
                        pol->triangles[3*i+2] );
                printf( "END\n\n" );
                        
        }
        struct dpol *ret = malloc( sizeof( struct dpol ) );
        ret->internal = pol;
        glGenVertexArrays( 1, &ret->vao );
        glBindVertexArray( ret->vao );
        glGenBuffers( 1, &ret->buffer );
        glBindBuffer( GL_ARRAY_BUFFER, ret->buffer );
        glBufferData( GL_ARRAY_BUFFER, sizeof( double ) * 2 * pol->num_points,
                      pol->points, GL_STATIC_DRAW );
        glVertexAttribPointer( 0, 2, GL_DOUBLE, GL_FALSE,
                               0, BUFFER_OFFSET( 0 ) );
        glEnableVertexAttribArray( 0 );

        glBindVertexArray( 0 );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        return ret;
}
