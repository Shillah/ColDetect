#version 430 core
#pragma debug(on)

layout(location = 0) in vec4 vPosition;

void
main()
{
        gl_Position = vPosition;
}