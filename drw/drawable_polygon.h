#if !defined( DRAWABLE_POLYGON_H__ )
#define DRAWABLE_POLYGON_H__

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <util/std_def.h>

#include <geometry/polygon.h>

struct dpol
{
        GLuint vao; /* VERTEX_ATTRIBUTE_BUFFER */;
        GLuint buffer;
        struct polygon *internal;
};

struct dpol *
make_dpol( struct polygon *pol );
       

#endif /* DRAWABLE_POLYGON_H__ */

