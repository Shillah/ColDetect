#if !defined(POLYGON_H__)
#define POLYGON_H__

#include <util/std_def.h>

#include <geometry/triangle.h>
#include <geometry/point.h>

struct box
{
        struct point bot_left;//, *top_right;
        double width, height;
};

struct polygon
{
        /*
         * This is a list of indices of the array points.
         * Every three consecutive indices form a triangle of
         * the triangulation of the polygon.
         */
        index num_points;
        index *triangles;
        struct box *bounding_box;
        struct point *points;
};

struct polygon *
make_polygon_var( index num_points, ... );

struct polygon *
make_polygon( index num_points, struct point points[ num_points ] );

#endif /* POLYGON_H__ */
