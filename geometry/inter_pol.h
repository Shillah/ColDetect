#if !defined( INTER_POL_H__ )
#define INTER_POL_H__

#include <util/std_def.h>
#include <util/sorted_cyclic_list.h>

#include <geometry/point.h>

/*
 * An intermediate representation of a partition
 * of a polygon that makes algorithm application easier.
 * !IMPORTANT!: Does not have its own copy of points!
 */

struct inter_pol
{
        // the number of points
        index num_points;
        // the points
        struct point *points;
        // for each point a sorted list of edges from that point
        struct scl **scls;
        // num_diags[i] is the number of diagonals starting/ending at
        // point i
        index *num_diags;
        // number of added diagonals
        index num_additional_diagonals;
};

struct inter_pol *
make_inter_pol( index num_points, struct point points[num_points] );

struct inter_pol *
free_inter_pol( struct inter_pol *pol );

// the diagonal is assumed to be an interior diagonal
void
add_diagonal( struct inter_pol *pol, index i, index j );

void
remove_diagonal( struct inter_pol *pol, index i, index j );

index **
get_created_polygons( struct inter_pol *pol );

index
get_created_polygon_into( struct inter_pol *pol, index *array );

index *
get_created_polygon( struct inter_pol *pol, index *pol_size );

#endif /* INTER_POL_H__ */
