#include "mon_pol.h"

#include <assert.h>

#include <util/std_def.h>
#include <geometry/point.h>
#include <geometry/line.h>

enum position { MPOL_LEFT, MPOL_RIGHT };

/*
 * This struct should represent a
 * (y-) monotone polygon.
 */
struct mon_pol
{
        
        /*
         * The points of the polygon sorted downwards.
         */
        /*
         * number of points
         */
        size_t num_points;
        /*
         * pointer to the list of points.
         */
        struct point *points;
        index *point_indices;
        
        /*
         * Array of the indices [ i_1 ... i_n ] of the array points
         * such that [ x[i_1] ... x[i_n] ] is an array of (y-) downward
         * sorted array of points.
         */
        index *sorted;

        /*
         * Gives the position of every point in points in regards to
         * the line from the uppermost point to the lowermost point;
         * that is the line x[i_1] ... x[i_n].
         */
        enum position *positions;
};

static void
free_mon_pol( struct mon_pol *mpol )
{
        free( mpol->positions );
        free( mpol->sorted );
        free( mpol );
}

static void
_quick_sort( index left, index right,
             index *sorted,
             enum position *positions,
             struct point *points )
{
        if ( left >= right)
                return;
        index m = (left + right)/2;
        index cur = sorted[m];
        index from_left = left;
        index from_right = right;

#define IS_LOWER( p, q ) \
        (points[(p)].y == points[(q)].y ? \
         points[(p)].x > points[(q)].x : \
         points[(p)].y < points[(q)].y)
#define IS_HIGHER( p, q ) \
        (points[(p)].y == points[(q)].y ? \
         points[(p)].x < points[(q)].x : \
         points[(p)].y > points[(q)].y)

        while ( from_left < from_right )
        {
                /*
                 * we want to sort the array, such that the
                 * highest point is at index 0
                 */
                while ( from_left < right &&
                        IS_HIGHER(sorted[from_left], cur) )
                        ++from_left;
                while ( from_right > left &&
                        IS_LOWER(sorted[from_right], cur)
                        && from_right != 0 )
                        --from_right;

                //
                // Swap from_left and from_right while updating
                // positions
                //

                if ( from_left >= from_right )
                        break;
                
                index li = sorted[from_left];
                enum position lpos = positions[from_left];
                index ri = sorted[from_right];
                enum position rpos = positions[from_right];

                sorted[from_right] = li;
                positions[from_right] = lpos;
                sorted[from_left] = ri;
                positions[from_left] = rpos;

                if ( m == from_left )
                        m = from_right;
                else if ( m == from_right )
                        m = from_left;
//                ++from_left;
//                --from_right;
        }

        // since right is unsigned we need to ensure not
        // to produce negative numbers
        if ( m > 0 )
        {
                _quick_sort( left, m-1, sorted, positions, points );
        }
        
        _quick_sort( m+1, right, sorted, positions, points );


#undef IS_LOWER
#undef IS_HIGHER

}

/*
 * Not every vertex should lay on the same line!
 */
static struct mon_pol *
make_mon_pol( index num_points,
              index point_indices[num_points],
              struct point *pol )
{
        struct mon_pol *mpol = NULL;
        if ( num_points < 1 )
        {
                return mpol;
        }
        else 
        {
                mpol = malloc( sizeof( struct mon_pol ) );
                index TOP = 0, BOT = 0;
                double miny = pol[point_indices[BOT]].y;
                double maxy = pol[point_indices[TOP]].y;
                mpol->points = pol;
                mpol->point_indices = point_indices;
                /* 
                 * First: find the top-most and bottom-most
                 * points of pol.
                 */
                for ( index i = 1; i < num_points; ++i )
                {
                        double cury = pol[point_indices[i]].y;
                        /*
                         * The asymmetry in this comparision is important!
                         * This ensures that TOP != BOT if n > 1
                         */
                        if ( miny > cury )
                        {
                                BOT = i;
                                miny = cury;
                        }
                        if ( maxy <= cury )
                        {
                                TOP = i;
                                maxy = cury;
                        }
                }

                enum position *positions = malloc( sizeof( enum position ) *
                                                   num_points );
                
                /*
                 * Third: Assign left/right to every point
                 */

#define OTHER_DIR( x ) \
                ((x) == MPOL_RIGHT ? MPOL_LEFT : MPOL_RIGHT)

                // find the direction we are going:

                enum lpos direction = ON_LINE;
                int change = 0;
                for ( index i = (BOT+1); i < (BOT+1) + num_points; ++i )
                {
                        index ri = i % num_points;
                        direction = position( pol[point_indices[BOT]],
                                              pol[point_indices[TOP]],
                                              pol[point_indices[ri]]);
                        if ( ri == TOP % num_points )
                                change = 1;
                        if ( direction != ON_LINE )
                                break;
                }
                enum position cur = (direction == LEFT_OF_LINE) ?
                        MPOL_LEFT : MPOL_RIGHT;
                if ( change )
                        cur = OTHER_DIR( cur );
                for ( index i = (BOT+1); i < (BOT+1) + num_points; ++i )
                {
                        index ri = i % num_points;
                        // i is our index modulo num_points
                        positions[ri] = cur;
                        // every point after top is assigned LEFT
                        if ( TOP % num_points == ri )
                                cur = OTHER_DIR( cur );

                }
#undef OTHER_DIR
                
                /*
                 * Last: Sort pol while updating TOP/BOT and positions
                 */

                mpol->sorted = malloc( sizeof( index ) * num_points );
                for ( index i = 0; i < num_points; ++i )
                {
                        mpol->sorted[i] = point_indices[i];
                }
                _quick_sort( 0, num_points-1, mpol->sorted,
                             positions, mpol->points);

                mpol->positions = positions;
                mpol->num_points = num_points;
        }

}

static void
_triangulize_monotone_polygon_into( struct mon_pol *mpol,
                                    index triangle_indices[] )
{
        index n = mpol->num_points;
        assert( n >= 3 );
        index stack[n];
        // stack index
        index si = 0;

#define EMPTY_STACK() (si == 0)
#define JUST_ONE() (si == 1)
#define PUSH( x ) do { stack[si++] = x; } while (0)
#define POP() (stack[--si])
#define DISCARD() (--si)
#define PEEK() (stack[si-1])

        // every triangulation of a polygon with n vertices
        // consists of exactly n-2 triangles
        index tn = n-2;
        // index *triangle_indices = malloc( sizeof( index ) * 3 * tn );
        // triangle index
        index ti = 0;

        ///enum position lpos = mpol->positions[0];
        enum position cpos = mpol->positions[1];
        PUSH(mpol->sorted[0]);
        PUSH(mpol->sorted[1]);

        // FROM THIS POINT ON THE STACK SHOULD ALWAYS CONTAIN >= 2
        // ELEMENTS!!
        
        for ( index i = 2; i < n; ++i )
        {
                enum position pos = mpol->positions[i];
                index point_index = mpol->sorted[i];

                if ( EMPTY_STACK() || JUST_ONE() )
                {
                        assert( 0 );
                        PUSH(point_index);
                        cpos = pos;
                }
                
                if ( pos != cpos )
                {
                        // we need to push this later!
                        index to_push = PEEK();
                        
                        while ( !JUST_ONE() )
                                // IF there is only one left
                                // then we already made a triangle
                                // with that point
                                // and can discard him
                        {
                                index second = POP();
                                index third = PEEK();
                                triangle_indices[3*ti] = point_index;
                                triangle_indices[3*ti+1] = second;
                                triangle_indices[3*ti+2] = third;
                                ti = ti + 1;

                        }
                        // discard the last point
                        DISCARD();
                        // now push the saved point and the new point
                        PUSH( to_push );
                } else {
                        // this will get complicated!

                        int could_see = 1;
                        
                        do
                        {
                                index second = POP();
                                index third = PEEK();

                                /* if you can see the third point from the
                                 * position (that is the line
                                 * point_index - third is inside the polygon)
                                 * then add the triangle point_index, second,
                                 * third since you can see the second point
                                 * trivially  you can see the point third <=>
                                 * we are on the right (left) and second on the
                                 * right (left) of the line  point_index - third.
                                 */ 

                                // on which side are we on atm ? (pos == cpos)
                                enum lpos side = (pos == MPOL_RIGHT) ?
                                        RIGHT_OF_LINE : LEFT_OF_LINE;

                                // see the /* - */ commentar for an explanation
                                if ( position( mpol->points[point_index],
                                               mpol->points[third],
                                               mpol->points[second] ) == side )
                                {
                                        triangle_indices[3*ti] = point_index;
                                        triangle_indices[3*ti+1] = second;
                                        triangle_indices[3*ti+2] = third;
                                        ti = ti + 1;
                                } else {
                                        // we couldnt form a new triangle,
                                        // so we push second again and go on
                                        could_see = 0;
                                        PUSH( second );
                                }
                                // stop when we cant form a triangle or
                                // we emptied the stack
                        } while ( could_see > 0 && !JUST_ONE() );
                }
                
                PUSH( point_index );
                cpos = pos;
                                        
        }
        
//        return triangle_indices;
}

void
mon_pol_triangulate_into( struct point points[],
                          index monotone_polygon[],
                          index num_points, // in the monotone polygon
                          index triangles[],
                          index start )
{
        //struct point tmp[num_points];
        //for ( index i = 0; i < num_points; ++i )
        //{
        //        tmp[i] = points[monotone_polygon[i]];
        //}
        struct mon_pol *mpol = make_mon_pol( num_points,
                                             monotone_polygon,
                                             points );

        _triangulize_monotone_polygon_into( mpol, triangles+3*start );
        
        free_mon_pol( mpol );
}

index *
mon_pol_triangulate( index num_points,
                     struct point monotone_pol[num_points] )
{
        index pi[num_points];
        for ( index i = 0; i< num_points; ++i )
        {
                pi[i] = i;
        }
        struct mon_pol *mpol = make_mon_pol( num_points,
                                             pi,
                                             monotone_pol );
        index *ret = malloc( sizeof( index ) * 3 * (num_points-2) );
        _triangulize_monotone_polygon_into( mpol, ret );
        free_mon_pol( mpol );
        return ret;
                                            
}
/*#if defined( DEBUG_MON_POL )

#include <stdio.h>

static void
print_point( point_t p )
{
        printf("(%lf, %lf)", p->x, p->y);
}       

void
main( void )
{
#define DOUBLE(x) ((double) x)
        struct point p1 = { DOUBLE(0), DOUBLE(4) };
        struct point p2 = { DOUBLE(-1), DOUBLE(2) };
        struct point p3 = { DOUBLE(0), DOUBLE(0) };
        struct point p4 = { DOUBLE(1), DOUBLE(1) };
        struct point p5 = { DOUBLE(1), DOUBLE(3) };
#undef DOUBLE

        size_t num_points = 5;
        
        struct point points[] = { p1, p2, p3, p4, p5 };

        struct mon_pol *mpol = make_mon_pol( num_points, points );

        for (int i = 0; i < num_points; ++i )
        {
                struct point p = points[mpol->sorted[i]];
                printf("(%lf, %lf) %s\n", p.x, p.y,
                       mpol->positions[i] == MPOL_RIGHT ? "RIGHT" : "LEFT");
        }
        
        int *triangles = _triangulize_monotone_polygon( mpol );
        for ( int i = 0; i < num_points-2; ++i )
        {
                print_point( &points[ triangles[3*i+0] ] );
                printf(" ");
                print_point( &points[ triangles[3*i+1] ] );
                printf(" ");
                print_point( &points[ triangles[3*i+2] ] );
                puts("");
        }
}

#endif /* DEBUG_MON_POL */
