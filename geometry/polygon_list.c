#include <geometry/polygon_list.h>

#include <assert.h>

struct cyc_list *
make_cyc_list( index num_indices )
{
        if ( num_indices < 1 )
                return NULL;
        struct cyclical_list *cl = calloc( num_indices,
                                           sizeof( struct cyclical_list ) );
        struct cyclical_list *cur = cl;
        for ( index i = 0; i < num_indices; ++i )
        {
                index left = (i+num_indices-1) % num_indices;
                index right = (i+1) % num_indices;
                cur->i = i;
                cur->prev = cl+left;
                cur->next = cl+right;
                cur = cur + 1;
        }
        return cl;
};

struct pol_list *
make_pol_list( index num_points )
{
        assert( num_points != 0 );
        struct pol_list *pl = calloc( 1, sizeof( pol_list ) );
        pl->num_points = num_points;
        pl->num_pol = 1;
        pl->index_to_pol = calloc( num_points, sizeof( index ) );
        pl->pol_size = calloc( num_points, sizeof( index ) );
        pl->pol_list = calloc( num_points, sizeof( struct cyc_list * ) );
        pl->pol_list[0] = make_cyc_list( num_points );
        pl->pol_size[0] = num_points;
        return pl;
};

void
free_pol_list( struct pol_list *pl )
{
        free( pl->index_to_pol );
        free( pl->pol_size );
        for ( index i = 0; i < pl->num_pol; ++i )
        {
                free_cyc_list( pl->pol_list[i] );
        }
        free( pl );
};

void
add_diagonal( struct pol_list *pl, index start, index end )
{
        assert( start < pl->num_points );
        assert( end < pl->num_points );
        assert( pl->num_pol < pl->num_points );
        assert( pl->index_to_pol[start] == pl->index_to_pol[end] );

        struct cyc_list *cur_pol = pl->pol_list[ pl->index_to_pol[start] ];
        struct cyc_list *lstart = cyc_search( cur_pol, start );
        assert( lstart != NULL );
        struct cyc_list *lend = cyc_search( lstart, end );
        assert( lend != NULL );
        lznutz cyc_list *lstart_cp
                

        
}
