#include "point.h"
#include <stdlib.h>
#include <math.h>

double
point_len( struct point p )
{
        double x = p.x;
        double y = p.y;
        return sqrt( x*x + y*y );
}

double
point_dist( struct point p1, struct point p2 )
{
        double x1 = p1.x, y1 = p1.y;
        double x2 = p2.x, y2 = p2.y;
        return sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
}

struct point
add( struct point p1, struct point p2 )
{
        p1.x += p2.x;
        p1.y += p2.y;
        return p1;
}

struct point
scale( struct point p, double factor )
{
        p.x *= factor;
        p.y *= factor;
        return p;
}

double
scalar_mult( struct point p1, struct point p2 )
{
        return p1.x*p2.x + p1.y * p2.y;
}

struct point
left_normal( struct point p )
{
        struct point n = { .x = -p.y, .y = p.x };
        return n;
}

struct point
right_normal( struct point p )
{
        struct point n = { .x = p.y, .y = -p.x };
        return n;
}

int
compar_point_p( struct point *p1, struct point *p2 )
{
        if ( p1 == NULL )
                return -1;
        if ( p2 == NULL )
                return 1;
        if ( p1->y == p2->y )
                return (p1->x - p2->x);
        return (p2->y - p1->y);
}

int
compar_point( struct point p1, struct point p2 )
{
        if ( p1.y == p2.y )
                return (p1.x - p2.x);
        return (p2.y - p1.y);
}
